
var canvas = document.getElementById('mainCanvas');
var ctx = canvas.getContext("2d");


ctx.lineWidth = 10;
ctx.lineCap = "round";

var mode;

var fontSize = 10;
var inputTXT = "untitled";
var fontFont = "Georgia";

var mousePos = [0,0];
var RGB = [0,0,0];


/*refresh mouse position*/
canvas.addEventListener('mousemove',
function(event){
    mousePos[0] = event.offsetX;
    mousePos[1] = event.offsetY;

    document.getElementById("id_mouseLocation").innerHTML=mousePos.toString();
});
/*refresh mouse position END*/

/*clear canvas*/
document.getElementById('btn_reset').addEventListener('click',
function(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
});
/*clear canvas END*/



document.getElementById('font1').addEventListener('click',
function(){
    document.getElementById('menu1').innerHTML = document.getElementById('font1').innerHTML;
    fontFont = document.getElementById('font1').innerHTML;
});
document.getElementById('font2').addEventListener('click',
function(){
    document.getElementById('menu1').innerHTML = document.getElementById('font2').innerHTML;
    fontFont = document.getElementById('font2').innerHTML;
});



function mode_arbi(){
    mode = "arbi";
}

function mode_line(){

    mode = "line";
}

function mode_eraser(){

  
    mode = "eraser";  
}

function mode_text(){
    mode = "text";
}

function mode_rect(){
    mode = "rect";
}

function mode_circ(){
    mode = "circ";
}



canvas.addEventListener('mouseover',
function(){


if (mode == "arbi" ){

    /*mouse down event*/
    canvas.addEventListener('mousedown',
    function(event){
        canvas.style.cursor = "crosshair";
        ctx.beginPath();
        ctx.moveTo(mousePos[0],mousePos[1]);    
        resetColor();
        
        canvas.addEventListener('mousemove',draw_arbi);        
    });
    /*mouse down event END*/

    /*draw arbi*/
    function draw_arbi(){
        ctx.lineTo(mousePos[0],mousePos[1]);
        ctx.stroke();
    }
    /*draw arbi END*/

    /*mouse up event*/
    canvas.addEventListener('mouseup',
    function(event){
        canvas.removeEventListener('mousemove',draw_arbi);        
        cPush();
        canvas.style.cursor = "default";
    });
    /*mouse up event END*/

}
else if (mode == "line"){
    
    var startPt = [0,0];
    var originPic;

     /*mouse down event*/
    canvas.addEventListener('mousedown',
    function(event){
        
        canvas.style.cursor = "none";
        startPt[0] = mousePos[0];
        startPt[1] = mousePos[1];    
        resetColor();
        

        canvas.addEventListener('mousemove',draw_line);        
    });
    /*mouse down event END*/

    /*draw line*/
    function draw_line(){
        
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        
        var img = new Image();
        img.src = cStack[cStack.length-1];
        img.onload = function(){ ctx.drawImage(img, 0, 0); }
       
        ctx.moveTo(startPt[0],startPt[1]);
        ctx.lineTo(mousePos[0],mousePos[1]);
        ctx.stroke();
    }
    /*draw line END*/

    /*mouse up event*/
    canvas.addEventListener('mouseup',
    function(event){
        ctx.beginPath();
        canvas.removeEventListener('mousemove',draw_line);        
        cPush();
        canvas.style.cursor = "default";
    });
    /*mouse up event END*/

}
else if (mode == "eraser"){

    /*mouse down event*/
    canvas.addEventListener('mousedown',
    function(event){

        canvas.style.cursor = "url";
        ctx.beginPath();
        ctx.moveTo(mousePos[0],mousePos[1]);    
        ctx.strokeStyle = "rgb(255,255,255)";
        ctx.fillStyle = "rgb(255,255,255)";
        
        canvas.addEventListener('mousemove',eraser);        
    });
    /*mouse down event END*/

    /*eraser*/
    function eraser(){
        ctx.lineTo(mousePos[0],mousePos[1]);
        ctx.stroke();
    }
    /*eraser END*/

    /*mouse up event*/
    canvas.addEventListener('mouseup',
    function(event){
        canvas.removeEventListener('mousemove',eraser);        
        cPush();
        canvas.style.cursor = "default";
    });
    /*mouse up event END*/

}
else if (mode == "text"){
    
    canvas.addEventListener('click',draw_text);
    function draw_text(){
       
        canvas.style.cursor = "text";
        inputTXT = document.getElementById('inputtxt').value
        ctx.font = fontSize + "px " + fontFont;
        ctx.fillText(inputTXT,mousePos[0],mousePos[1]);
    
    }
        canvas.removeEventListener('mousemove',draw_text);        
}
else if (mode == "rect"){
    
    var startPt = [0,0];
    var originPic;

     /*mouse down event*/
    canvas.addEventListener('mousedown',
    function(event){
        
        startPt[0] = mousePos[0];
        startPt[1] = mousePos[1];    
        resetColor();
         

        canvas.addEventListener('mousemove',draw_rect);        
    });
    /*mouse down event END*/

    /*draw line*/
    function draw_rect(){
        
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        
        var img = new Image();
        img.src = cStack[cStack.length-1];
        img.onload = function(){ ctx.drawImage(img, 0, 0); }
       
        ctx.rect(startPt[0],startPt[1],mousePos[0]-startPt[0],mousePos[1]-startPt[1]);
        ctx.stroke();
    }
    /*draw line END*/

    /*mouse up event*/
    canvas.addEventListener('mouseup',
    function(event){
        ctx.beginPath();
        canvas.removeEventListener('mousemove',draw_rect);        
        cPush();
        canvas.style.cursor = "default";
    });
    /*mouse up event END*/

}
else if (mode == "circ"){
    
    var startPt = [0,0];
    var originPic;

     /*mouse down event*/
    canvas.addEventListener('mousedown',
    function(event){
        
        canvas.style.cursor = "grab";
        startPt[0] = mousePos[0];
        startPt[1] = mousePos[1];    
        resetColor();
         

        canvas.addEventListener('mousemove',draw_circ);        
    });
    /*mouse down event END*/

    /*draw line*/
    function draw_circ(){
        
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        
        var img = new Image();
        img.src = cStack[cStack.length-1];
        img.onload = function(){ ctx.drawImage(img, 0, 0); }
       
        ctx.arc(startPt[0],startPt[1],eval(((startPt[0]-mousePos[0])^2+(startPt[1]-mousePos[1])^2)^0.5),0,2*Math.PI);
        ctx.stroke();
    }
    /*draw line END*/

    /*mouse up event*/
    canvas.addEventListener('mouseup',
    function(event){
        ctx.beginPath();
        canvas.removeEventListener('mousemove',draw_circ);        
        cPush();
        canvas.style.cursor = "default";
    });
    /*mouse up event END*/

}


});







/*RGB selector*/
var slider_rgbR = document.getElementById("rgbRange_r");
var output_rgbR = document.getElementById("rgbValue_r");
output_rgbR.innerHTML = slider_rgbR.value;

var slider_rgbG = document.getElementById("rgbRange_g");
var output_rgbG = document.getElementById("rgbValue_g");
output_rgbG.innerHTML = slider_rgbG.value;

var slider_rgbB = document.getElementById("rgbRange_b");
var output_rgbB = document.getElementById("rgbValue_b");
output_rgbB.innerHTML = slider_rgbB.value;

slider_rgbR.oninput = function() {
  RGB[0] = this.value;
  output_rgbR.innerHTML = RGB[0];
}
slider_rgbG.oninput = function() {
  RGB[1] = this.value;
  output_rgbG.innerHTML = RGB[1];
}
slider_rgbB.oninput = function() {
  RGB[2] = this.value;
  output_rgbB.innerHTML = RGB[2];
}
/*RGB selector END*/

/*reset color*/
function resetColor(){
    ctx.strokeStyle = "rgb(" + RGB[0] + "," + RGB[1] + "," + RGB[2] + ")" ;
    ctx.fillStyle = "rgb(" + RGB[0] + "," + RGB[1] + "," + RGB[2] + ")" ;
}
/*reset color END*/



/* set pen, eraser, font size*/
var slider_penSize = document.getElementById("penSize_Range");
var output_penSize = document.getElementById("penSizeValue");
output_penSize.innerHTML = slider_penSize.value;
fontSize = ctx.lineWidth;

slider_penSize.oninput = function() {
  ctx.lineWidth = this.value;
  output_penSize.innerHTML = ctx.lineWidth;
  fontSize = ctx.lineWidth;
}
/*set pen size END*/



/*download & upload*/
function download() {
    var download = document.getElementById("download");
    var image = canvas.toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);

}

document.getElementById('upload').onchange = function(e) {
  var img = new Image();
  img.onload = function(){ ctx.drawImage(this, 0, 0); }
  img.onerror = function(){ alert('upload failed'); }
  img.src = URL.createObjectURL(this.files[0]);
};
/*download & upload END*/



/*undo & redo*/
var cStack = new Array();
var cStep = -1;
	
function cPush() {
    cStep++;
    if (cStep < cStack.length) { cStack.length = cStep; }
    cStack.push(canvas.toDataURL());
}

function cUndo() {
    if (cStep > 0) {        
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        cStep--;
        var img = new Image();
        img.src = cStack[cStep];
        img.onload = function(){ ctx.drawImage(img, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cStack.length-1) {    
        ctx.clearRect(0, 0, canvas.width, canvas.height);     
        cStep++;
        var img = new Image();
        img.src = cStack[cStep];
        img.onload = function () { ctx.drawImage(img, 0, 0); }
    }
}
/*undo & redo END*/









